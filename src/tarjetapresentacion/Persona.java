/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarjetapresentacion;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author actuaria
 */
public class Persona {
    private String nombre;
    private short  edad;
    private char sexo;
    private String nacionalidad;
    private String ocupacion;
    private String correo;
    private int numero;
    private String redSocial;
    private Object fr;

    Persona(String archivo) {
      Leer(archivo);
    }
    //métodos que  
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getEdad() {
        return edad;
    }

    public void setEdad(short edad) {
        this.edad = edad;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getRedSocial() {
        return redSocial;
    }

    public void setRedSocial(String redSocial) {
        this.redSocial = redSocial;
    }
    
  
    private void Leer(String archivo){
          FileReader fr = null;
          BufferedReader br = null;
        try{
             fr= new FileReader(archivo); //Sirve para poder escribir los valores desde el teclado
             br = new BufferedReader (fr);
            
            String nom = br.readLine();
            this.nombre = nom;
            String ed = br.readLine();
            this.edad = Short.parseShort(ed);
            String temp = br.readLine().substring(0, 1).toLowerCase();
            if(temp.equals("m")||temp.equals("f")){
                this.sexo=temp.charAt(0);
            }else{ 
               this.sexo = ' '; 
            }
            String corr = br.readLine();
            this.correo = corr;
            int num = Integer.parseInt("5523040050");
            String redSocial = br.readLine();
            this.redSocial = redSocial;
            } catch (Exception e){
                System.err.print("Error en el archivo");
                System.exit (0);
            }finally{
            try{
                if (null !=fr){
                 fr.close();   
                }
            }catch(Exception e2){
                System.err.print("Error con el archivo");
                System.exit(0);
            }
        }
    }
            public void mostrarTarjeta(){
                
                System.out.println(".----------------------------------------.");
                System.out.println("|Nombre:"+getNombre()+                  "|");
                System.out.println("|----------------------------------------|");
                System.out.println("|Edad  :"+getEdad()+                    "|");
                System.out.println("|----------------------------------------|");
                if(getSexo() !='f'){
                System.out.println("|Sexo  :Masculino                        |");
                }else{
                System.out.println("|Sexo  :Femenino                         |");   
                }
                System.out.println("|----------------------------------------|"); 
                System.out.println("|Nacionalidad:"+getNacionalidad()+      "|");
                System.out.println("|----------------------------------------|"); 
                System.out.println("|Ocupación   :"+getOcupacion()+         "|");
                System.out.println("|----------------------------------------|"); 
                System.out.println("|Correo      :"+getCorreo()+            "|");
                System.out.println("|----------------------------------------|"); 
                System.out.println("|Red Social  :"+getRedSocial()+         "|");
                System.out.println("|----------------------------------------|"); 
                System.out.println("|Número:"+getNumero()+                  "|"); 
                System.out.println(".----------------------------------------.");
                
            }
         
}

     



        
        
          
     
    
     
